from turtle import Turtle


class Score(Turtle):
    def __init__(self):
        super().__init__()
        self.s_score = 0
        # read the high-score from file (if exists)
        try:
            with open("highscore.txt", "r") as f_score:
                self.high_score = int(f_score.read())
        except FileNotFoundError:
            print("File 'highscore.txt' not found, using static 0.")
            self.high_score = 0

        self.goto(0, 280)
        self.color("white")
        self.hideturtle()
        self.update_scoreboard()

    def update_scoreboard(self):
        self.clear()
        self.write(f"Score: {self.s_score} High Score: {self.high_score}",
                   False, align="center", font=("Arial", 18, "normal"))

    def reset_board(self):
        if self.s_score > self.high_score:
            try:
                with open("highscore.txt", "w") as f_score:
                    f_score.write(str(self.s_score))
            except FileNotFoundError:
                print("Could not open file.")
            finally:
                f_score.close()

            self.high_score = self.s_score
        self.s_score = 0
        self.update_scoreboard()

    def increase_score(self):
        self.s_score += 1
        self.update_scoreboard()
