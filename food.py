from turtle import Turtle
import random
FOOD_COLOR = "green"

class Food(Turtle):

    def __init__(self):
        super().__init__()
        self.shape("circle")
        self.pu()
        self.shapesize(stretch_len=0.7, stretch_wid=0.7)
        self.color(FOOD_COLOR)
        self.speed("fastest")
        self.refresh()

    def refresh(self):
        randX = random.randint(-270, 270)
        randY = random.randint(-270, 270)
        self.goto(randX, randY)
