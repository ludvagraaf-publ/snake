# snake

SNAKE is a retro game for singe player. Is written in Python 3.10. Try keep 
the snake from colliding with other obstacles and itself, which gets harder
 as the snake lengthens.


## Installation

It uses [Tk interface package](https://docs.python.org/3/library/tkinter.html#module-tkinter).
Use the package manager [pip](https://pip.pypa.io/en/stable/) to install tkinter.

```bash
pip install tkinter
```

On older macOS (BigSur) you will need to install it from Brew.
Install [brew](https://brew.sh/) first if you haven't done so already.
Then install tkinter for specific version of Python (eg 3.10):

```bash
brew install python-tk@3.10
```

## Usage

In the directory where you download/clone:

```bash
cd ~/Download/snake/
python ./main.py
```

### Playing the game:

The "head" of the snake continually moves forward, unable to stop, growing ever
 longer. It must be steered left, right, up, and down to avoid hitting walls
and the body of either snake. Controls are "Up/Down/Left/Right" arrows. 
For quit press "q".

Enjoy the game!

