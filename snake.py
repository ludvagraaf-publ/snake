from turtle import Turtle

SNAKE_MOVEMENT = 20
SNAKE_COLOR = "yellow"


class Snake:
    """ The all operations with snake itself"""
    def __init__(self):
        self.segments = []
        self.create_snake()
        self.head = self.segments[0]

    def create_snake(self):
        self.s_pos = [(0, 0), (-20, 0), (-40, 0)]
        for ss in self.s_pos:
            self.add_segment(ss)

    def extent_segment(self):
        self.add_segment(self.segments[-1].position())

    def add_segment(self,ss):
        self.ss = ss
        snake = Turtle("square")
        snake.color(SNAKE_COLOR)
        snake.pu()
        snake.goto(ss)
        self.segments.append(snake)

    def reset_snake(self):
        for seg in self.segments:
            seg.goto(1000, 1000)
        self.segments.clear()
        self.create_snake()
        self.head = self.segments[0]

    def move(self):
        for seg_num in range(len(self.segments) - 1, 0, -1):
            new_x = self.segments[seg_num - 1].xcor()
            new_y = self.segments[seg_num - 1].ycor()
            self.segments[seg_num].goto(new_x, new_y)
        self.segments[0].forward(SNAKE_MOVEMENT)

    def right(self):
        self.head.speed(0)
        if self.head.heading() != 180:
            self.head.setheading(0)

    def up(self):
        self.head.speed(0)
        if self.head.heading() != 270:
            self.head.setheading(90)

    def left(self):
        self.head.speed(0)
        if self.head.heading() != 0:
            self.head.setheading(180)

    def down(self):
        self.head.speed(0)
        if self.head.heading() != 90:
            self.head.setheading(270)
