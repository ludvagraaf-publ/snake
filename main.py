from turtle import Screen
from snake import Snake
from food import Food
from score import Score
import time

# initial snake speed
SNAKE_SPEED = 0.2

screen = Screen()
screen.setup(width=600, height=600)
screen.bgcolor("black")
screen.title("The Snake game")
screen.tracer(0)


def blink_screen():
    """ when reset snake - use also visual effect """
    screen.bgcolor("white")
    time.sleep(0.05)
    screen.bgcolor("black")


snake = Snake()
food = Food()
score = Score()

screen.listen()
screen.onkeypress(snake.up, "Up")
screen.onkeypress(snake.down, "Down")
screen.onkeypress(snake.left, "Left")
screen.onkeypress(snake.right, "Right")
screen.onkeypress(screen.bye, "q")

game_on = True
while game_on:
    screen.update()
    time.sleep(SNAKE_SPEED)
    snake.move()

    # detect eating food
    if snake.head.distance(food) < 15:
        score.increase_score()
        food.refresh()
        snake.extent_segment()

    # speedup the game
    if score.s_score == 5:
        SNAKE_SPEED = 0.1
    elif score.s_score == 10:
        SNAKE_SPEED = 0.07

    # collision with the wall
    if snake.head.xcor() > 290 or snake.head.xcor() < -290 or \
            snake.head.ycor() > 290 or snake.head.ycor() < -290:
        score.reset_board()
        blink_screen()
        snake.reset_snake()
        SNAKE_SPEED = 0.2

    # collision with snake's body
    for segm in snake.segments[1:]:
        if snake.head.distance(segm) < 10:
            score.reset_board()
            blink_screen()
            snake.reset_snake()
            SNAKE_SPEED = 0.2

screen.exitonclick()
